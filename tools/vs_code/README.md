# VS Code 编辑器

## settings.json 用户配置文件

路径:

`C:\Users\Administrator\AppData\Roaming\Code\User\settings.json`

[个人常用配置内容](./settings.json)

## keybindings.json 快捷键自定义配置

* [快捷键学习](./keybindings.md)

## 搜索命令面板

快捷键: `Ctrl + Shift + P`

### 命令列表

配置语言: `Configure Display Language`

## 常用插件

### 必装列表

* C/C++
* [C#](https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp)
* C# XML Documentation Comments
* vscode-solution-explorer
* vscode-icons
* Chinese (Simplified) Language Pack for Visual Studio Code
* Python
* [.NET Core Test Explorer](https://marketplace.visualstudio.com/items?itemName=formulahendry.dotnet-test-explorer)
* [Markdown-Notes-Pack](https://marketplace.visualstudio.com/items?itemName=OrangeX4.markdown-notes-pack)
* [Cmd Markdown 公式指导手册](https://www.zybuluo.com/codeep/note/163962)
* [Paste Image - 贴上图片](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image)
* [Markmap - Markdown文件转思维导图方式展示](https://marketplace.visualstudio.com/items?itemName=gera2ld.markmap-vscode)

### 收藏列表

* ESLint
* Markdownlint
* SQL Server(mssql)
* Debugger for Firefox
* Git History
* [Code Runner](https://github.com/formulahendry/vscode-code-runner)
* [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
* Beautify
* [draw.io 流程图绘制插件](https://github.com/hediet/vscode-drawio)
* [T4 Support](https://marketplace.visualstudio.com/items?itemName=zbecknell.t4-support)
* [TT-Processor](https://marketplace.visualstudio.com/items?itemName=aisoftware.tt-processor)
* [Compile Hero - 编译各类文件: Sass/Less/Stylus/Pug/Jade/Typescript/Javascript](https://marketplace.visualstudio.com/items?itemName=Wscats.eno)
* [Markdown All in One - 实现媲美Typora的Markdown编辑体验](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
* [LeetCode](https://marketplace.visualstudio.com/items?itemName=LeetCode.vscode-leetcode)
* [Live Sass Compiler](https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass)
